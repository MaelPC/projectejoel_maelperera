/**
 * Aquest paquet conté tots els arxius necessaris per el problema y el seus Tests.
 */
package problemajoel;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 * Aquest arxiu conté tots els test que donaran un input a la funcio i comprobaran si el resultat que dona la funcio és el esperat.
 */
class ProbesJoel {

    @Test
    public void test01() {
        int[][] expected = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int[][] result = Puzzle.solucio("5,6,1,2,3,4,8,9,7");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }      }

    @Test
    public void test02() {
        int[][] expected = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int[][] result = Puzzle.solucio("1,2,3,4,5,6,7,8,9");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }    
        }

    @Test
    public void test03() {
        int[][] expected = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int[][] result = Puzzle.solucio("9,8,7,6,5,4,3,2,1");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }        }

    @Test
    public void test04() {
        int[][] expected = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int[][] result = Puzzle.solucio("3,2,1,6,5,4,9,8,7");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }        }

    @Test
    public void test05() {
        int[][] expected = {{1, 1, 1}, {1, 1, 1}, {1, 1, 1}};
        int[][] result = Puzzle.solucio("1,1,1,1,1,1,1,1,1");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }        }
    
    @Test
    public void test06() {
        int[][] expected = {{2, 3, 5}, {6, 7, 8}, {9, 10, 12}};
        int[][] result = Puzzle.solucio("12,10,5,6,3,2,9,7,8");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }        }

    @Test
    public void test07() {
        int[][] expected = {{15, 20, 25}, {30, 35, 40}, {45, 50, 55}};
        int[][] result = Puzzle.solucio("50,55,20,30,25,15,35,45,40");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }        }

    @Test
    public void test08() {
        int[][] expected = {{100, 200, 300}, {400, 500, 600}, {700, 800, 900}};
        int[][] result = Puzzle.solucio("800,500,100,600,300,400,200,900,700");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }        }

    @Test
    public void test09() {
        int[][] expected = {{11, 13, 15}, {17, 19, 21}, {23, 25, 27}};
        int[][] result = Puzzle.solucio("25,21,11,19,13,15,27,23,17");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }        }
    
    @Test
    public void test10() {
        int[][] expected = {{31, 35, 39}, {43, 47, 51}, {55, 59, 63}};
        int[][] result = Puzzle.solucio("59,51,31,47,39,35,63,55,43");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }        }

    @Test
    public void test11() {
        int[][] expected = {{8, 16, 24}, {32, 40, 48}, {56, 64, 72}};
        int[][] result = Puzzle.solucio("64,48,24,56,16,8,40,32,72");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }        }

    @Test
    public void test12() {
        int[][] expected = {{21, 25, 29}, {33, 37, 41}, {45, 49, 53}};
        int[][] result = Puzzle.solucio("49,53,25,33,29,21,37,45,41");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }        }

    @Test
    public void test13() {
        int[][] expected = {{10, 20, 30}, {40, 50, 60}, {70, 80, 90}};
        int[][] result = Puzzle.solucio("80,50,10,60,30,40,20,90,70");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }        }
    
    @Test
    public void test14() {
        int[][] expected = {{-31, -25, -19}, {-13, -7, -3}, {1, 5, 11}};
        int[][] result = Puzzle.solucio("-7,1,-31,-3,-19,11,5,-13,-25");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }        }

    @Test
    public void test15() {
        int[][] expected = {{-8, -6, -4}, {-2, 0, 2}, {4, 6, 8}};
        int[][] result = Puzzle.solucio("0,2,-4,6,-8,4,-2,8,-6");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }        }

    @Test
    public void test16() {
        int[][] expected = {{-15, -10, -5}, {0, 5, 10}, {15, 20, 25}};
        int[][] result = Puzzle.solucio("10,0,-15,20,5,-5,15,25,-10");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }        }

    @Test
    public void test17() {
        int[][] expected = {{-21, -18, -15}, {-12, -9, -6}, {-3, 0, 3}};
        int[][] result = Puzzle.solucio("-9,-3,-18,0,-15,-21,3,-6,-12");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }        }

    @Test
    public void test18() {
        int[][] expected = {{-100, -90, -80}, {-70, -60, -50}, {-40, -30, -20}};
        int[][] result = Puzzle.solucio("-60,-20,-100,-50,-90,-40,-70,-30,-80");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }        }

    @Test
    public void test19() {
        int[][] expected = {{-11, -7, -3}, {1, 5, 9}, {13, 17, 21}};
        int[][] result = Puzzle.solucio("5,9,-11,-7,13,1,17,-3,21");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }        }

    @Test
    public void test20() {
        int[][] expected = {{-14, -10, -6}, {-2, 2, 6}, {10, 14, 18}};
        int[][] result = Puzzle.solucio("6,2,-14,14,-10,-6,10,-2,18");
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }        }
}
