/**
 * Aquest paquet conté tots els arxius necessaris per el problema y el seus Tests.
 */
package problemajoel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
/**
 * Aquest codi s'encarrega de convertir el input a la posible solucio esperada.
 */
public class Puzzle {
	
	/**
	 * El main conté una string y una crida a la funcio per comprobar que funcioni bé la funció.
	 * @param args Arguments de la línia de comandes (no s'utilitzen en aquest cas).
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		// El codi utilitza una string amb números separats per ,
	
		String ListaDesordenada = sc.nextLine();
		
		// Una crida a la funcio amb la string posada;
		solucio(ListaDesordenada);
	}
	

	/**
	 * Aquesta funció és la que és passa després al Test per tant de descobrir si el input a tingut el resultat esperat o no. 
	 *  @param ListaDesordenada La cadena de números desordenats separats per coma.
	 * @return Una matriu 3x3 amb els números ordenats de manera ascendent.
	 */
	public static int[][] solucio(String ListaDesordenada) {
		
		// Posa a un array la listaDesordenada(el String amb ,)
		String [] Array = ListaDesordenada.split(",");
				
		// Creem una llista que contindra cada numero
		ArrayList<Integer> ListaOrdenada = new ArrayList<Integer>();
		
		// Posem els numeros a la Llista
		for (int i = 0; i<9; i++) {
		
			ListaOrdenada.add(Integer.parseInt(Array[i]));
		}
		
		// inicialitzem el tamany de la matriu (Mai sera de més o menys de 3x3)
		int [][] MatrizDesordenada = new int [3][3];
		
		// ordenem els numeros de la llista
		Collections.sort(ListaOrdenada);
		
		// Un conmptador
		int pasar = 0;
		
		// Posa cada numero a la matriu amb el valor del comptador++ a la vegada que imprimeix la matriu
		for (int i = 0; i < MatrizDesordenada.length; i++) {
			for (int j = 0; j < MatrizDesordenada[0].length; j++) {
				MatrizDesordenada[i][j] = ListaOrdenada.get(pasar++);
				System.out.print(MatrizDesordenada[i][j] + " ");
			}
			System.out.println();
		}
		
		// Retorna la Matriu per fer la comprovació
		return MatrizDesordenada;
		
	}
	
}
